$(function() {
	
	var $page = $('html');
	$('.main-title__content a').click(function() {
	    $page.animate({
	        scrollTop: $($.attr(this, 'href')).offset().top
	    }, 700);
	    return false;
	});
	$('.btn-popup-1').click(function() {
		$('#popup1').modal({
			fadeDuration: 250,
		});
	});
	// $('.btn-popup-2').click(function() {
	// 	$('#popup2').modal({
	// 		fadeDuration: 250,
	// 	});
	// });
	$('.open-design__1').click(function() {
		$('.design-popup__1').modal({
			fadeDuration: 250
		});
		$('.slider-popup__1').slick({
			speed: 500,
  		  fade: true,
  		  cssEase: 'linear',
  		  prevArrow: '<div class="design-slider__prev"></div>',
		  nextArrow: '<div class="design-slider__next"></div>'
		});
	});
	$('.open-design__2').click(function() {
		$('.design-popup__2').modal({
			fadeDuration: 250
		});
		$('.slider-popup__2').slick({
		  speed: 500,
  		  fade: true,
  		  cssEase: 'linear',
  		  prevArrow: '<div class="design-slider__prev"></div>',
		  nextArrow: '<div class="design-slider__next"></div>'
		});
	});	
	$('.open-design__3').click(function() {
		$('.design-popup__3').modal({
			fadeDuration: 250
		});
		$('.slider-popup__3').slick({
			speed: 500,
  		  fade: true,
  		  cssEase: 'linear',
  		  prevArrow: '<div class="design-slider__prev"></div>',
		  nextArrow: '<div class="design-slider__next"></div>'
		});
	});
	$('.open-design__4').click(function() {
		$('.design-popup__4').modal({
			fadeDuration: 250
		});
		$('.slider-popup__4').slick({
			speed: 500,
  		  fade: true,
  		  cssEase: 'linear',
  		  prevArrow: '<div class="design-slider__prev"></div>',
		  nextArrow: '<div class="design-slider__next"></div>'
		});
	});
	$('.open-design__5').click(function() {
		$('.design-popup__5').modal({
			fadeDuration: 250
		});
		$('.slider-popup__5').slick({
			speed: 500,
  		  fade: true,
  		  cssEase: 'linear',
  		  prevArrow: '<div class="design-slider__prev"></div>',
		  nextArrow: '<div class="design-slider__next"></div>'
		});
	});
	$('.design-slider').slick({
			infinite: true,
			centerMode: true,
			centerPadding: '15%',
			slidesToShow: 1,
			prevArrow: '<div class="design-slider__prev"></div>',
		  	nextArrow: '<div class="design-slider__next"></div>'
	});
	$('.works-slider').slick({
		prevArrow: '<div class="design-slider__prev"></div>',
		 nextArrow: '<div class="design-slider__next"></div>'
	});
	$('.review-slider').slick({
		slidesToShow: 2,
		prevArrow: '<div class="design-slider__prev"></div>',
		 nextArrow: '<div class="design-slider__next"></div>',
		 responsive: [
		 {
		 	breakpoint: 992,
		 	settings: {
		 		slidesToShow: 1
		 	}
		 },
		 {
		 	breakpoint: 575,
		 	settings: {
		 		adaptiveHeight: true,
		 		slidesToShow: 1
		 	}
		 },
		 ]
	});
	$('.design').waypoint(function() {
		$('.btn-black').each(function(index) {
			var ths = $(this);
			setInterval(function() {
			 ths.addClass('on');
		}, 100*index);
		});
	});
	$('#popup1').waypoint(function() {
		$('.btn-form__1').each(function(index) {
			var ths = $(this);
			setInterval(function() {
			 ths.addClass('on');
		}, 100*index);
		});
	});
	$('.contacts-form').waypoint(function() {
		$('.btn-form__2').each(function(index) {
			var ths = $(this);
			setInterval(function() {
			 ths.addClass('on');
		}, 100*index);
		});
	});

	if(window.matchMedia('(max-width: 991px)').matches){
		$('.advantages-row').slick({
			speed: 500,
			arrows: false,
			slidesToShow: 2,
  		slidesToScroll: 2,
  		dots: true,
  		responsive: [{
		 	breakpoint: 575,
		 	settings: {
		 		slidesToShow: 1,
  			slidesToScroll: 1
		 	}
		 }]
		});
	}
	$('.hamburger-box').click(function(e) {
		$('#menu-item').show()
		$('.mobile-close-menu').show()
	});
	$('.mobile-close-menu').click(function(e) {
		$('#menu-item').hide()
		$(this).hide()
	});
});